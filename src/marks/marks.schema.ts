import Joi from "joi";
import { IMark } from "./types/marks.interface";

export const MarkCreateSchema = Joi.object<Omit<IMark, 'id'>>({
    mark: Joi.number().required(),
    studentId: Joi.string().required(),
    lectorId: Joi.string().required(),
    courseId: Joi.string().required(),
})