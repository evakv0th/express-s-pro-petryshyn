import { Request, Response } from 'express';
import * as marksService from './marks.service';
import { ValidatedRequest } from 'express-joi-validation';
import HttpStatusCode from '../application/enums/http-statuses.enums';
import { IMarkCreateRequest } from './types/mark-create.schema';

export const getStudentMarks = async (request: Request, response: Response) => {
  const studentId = request.query.student_id;
  const marks = await marksService.getStudentMarks(studentId as string);
  response.json(marks);
};

export const getCourseMarks = async (request: Request, response: Response) => {
    const courseId = request.query.course_id;
    const marks = await marksService.getCourseMarks(courseId as string);
    response.json(marks);
  };


export const assignMarkToStudent = async (
  request: ValidatedRequest<IMarkCreateRequest>,
  response: Response,
) => {
  const mark = await marksService.assignMarkToStudent(request.body);
  response
    .status(HttpStatusCode.CREATED)
    .json({ message: 'Created mark', mark: mark });
};
