import { Router } from 'express';
import * as groupsController from './groups.controller';
import validator from '../application/middlewares/validation.middleware';
import { controllerWrapper } from '../application/utilities/controller-wrapper';
import { groupCreateSchema, groupUpdateSchema } from './groups.schema';
import {
  idValidateForAddStudentToGroupSchema,
  idValidateSchema,
} from '../application/schemas/id-param.schema';

const router = Router();

router.get('/', controllerWrapper(groupsController.getAllGroups));
router.get(
  '/:id',
  validator.params(idValidateSchema),
  controllerWrapper(groupsController.getGroupById),
);
router.post(
  '/',
  validator.body(groupCreateSchema),
  controllerWrapper(groupsController.createGroup),
);
router.patch(
  '/:id',
  validator.params(idValidateSchema),
  validator.body(groupUpdateSchema),
  controllerWrapper(groupsController.updateGroupById),
);

router.delete(
  '/:id',
  validator.params(idValidateSchema),
  controllerWrapper(groupsController.deleteGroupById),
);


export default router;