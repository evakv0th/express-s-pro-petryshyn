import { NextFunction, Request, Response } from "express";
import HttpException from "../exceptions/http-exception";
import HttpStatusCode from "../enums/http-statuses.enums";

export const exceptionsFilter = (error: HttpException, request: Request, response:Response, next: NextFunction) => {
    const status = error.status || HttpStatusCode.INTERNAL_SERVER_ERROR;
    const message = error.message || 'Something went wrong';
    response.status(status).send({status, message});
}

