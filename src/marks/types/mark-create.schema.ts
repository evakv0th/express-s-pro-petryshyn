import {ContainerTypes, ValidatedRequestSchema} from 'express-joi-validation';
import { IMark } from './marks.interface';

export interface IMarkCreateRequest extends ValidatedRequestSchema {
    [ContainerTypes.Body]: Omit<IMark, 'id'>;
}