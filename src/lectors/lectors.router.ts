import { Router } from 'express';
import * as lectorsController from './lectors.controller';
import validator from '../application/middlewares/validation.middleware';
import { controllerWrapper } from '../application/utilities/controller-wrapper';
import {
  idValidateForAddLectorToCourseSchema,
  idValidateSchema,
} from '../application/schemas/id-param.schema';
import { lectorCreateSchema } from './lectors.schema'; 

const router = Router();

router.get('/', controllerWrapper(lectorsController.getAllLectors));
router.get(
  '/:id',
  validator.params(idValidateSchema),
  controllerWrapper(lectorsController.getLectorById),
);
router.get(
  '/:id/courses',
  validator.params(idValidateSchema),
  controllerWrapper(lectorsController.getLectorCourses),
);
router.post(
  '/',
  validator.body(lectorCreateSchema),
  controllerWrapper(lectorsController.createLector),
);
router.patch(
  '/:id/courses/:courseId',
  validator.params(idValidateForAddLectorToCourseSchema),
  controllerWrapper(lectorsController.addLectorToCourse),
);


export default router;