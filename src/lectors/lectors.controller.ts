import { Request, Response } from 'express';
import * as lectorsService from './lectors.service';
import { ValidatedRequest } from 'express-joi-validation';
import HttpStatusCode from '../application/enums/http-statuses.enums';
import { ILectorCreateRequest } from './types/lectors-create.schema';

export const getAllLectors = async (request: Request, response: Response) => {
  const lectors = await lectorsService.getAllLectors();
  response.json(lectors);
};

export const getLectorById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const lector = await lectorsService.getLectorById(id);
  response.json(lector);
};

export const createLector = async (
  request: ValidatedRequest<ILectorCreateRequest>,
  response: Response,
) => {
  const lector = await lectorsService.createLector(request.body);
  response
    .status(HttpStatusCode.CREATED)
    .json({ message: 'Created lector', lector: lector });
};

export const addLectorToCourse = async (request: Request, response: Response) => {
  const { id, courseId } = request.params;
  const lector = await lectorsService.addLectorToCourse(id, courseId);
  response.json({message: `successfully added lector to course`, lector: lector});
};


export const getLectorCourses = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const courses = await lectorsService.getLectorCourses(id);
  response.json(courses);
};