import { Request, Response } from 'express';
import * as studentsService from './students.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IStudentUpdateRequest } from './types/student-update-request.interface';
import { IStudentCreateRequest } from './types/student-create-request.interface';
import HttpStatusCode from '../application/enums/http-statuses.enums';

export const getAllStudents = async (request: Request, response: Response) => {
  const { name } = request.query;
  const students = await studentsService.getAllStudents(name as string);
  response.json(students);
};

export const getStudentById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const student = await studentsService.getStudentById(id);
  response.json(student);
};

export const createStudent = async (
  request: ValidatedRequest<IStudentCreateRequest>,
  response: Response,
) => {
  const student = await studentsService.createStudent(request.body);
  response.status(HttpStatusCode.CREATED).json({message: `student created`, student: student});
};

export const updateStudentById = async (
  request: ValidatedRequest<IStudentUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const student = await studentsService.updateStudentById(id, request.body);
  response.json({message: "updated student info", student: student});
};

export const deleteStudentById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const student = await studentsService.deleteStudentById(id);
  response.json(student);
};

export const addStudentToGroup = async (request: Request, response: Response) => {
  const { id, groupId } = request.params;
  const student = await studentsService.addStudentToGroup(id, groupId);
  response.json({message: `successfully added student to group`, student: student});
};

