import basicAuth from 'express-basic-auth';
import dotenv from 'dotenv';

dotenv.config();

const username = process.env.APP_USERNAME as string;
const password = process.env.PASSWORD as string;


const auth = basicAuth({
    users: {[username]: password}
})

export default auth;
