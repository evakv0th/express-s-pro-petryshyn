import HttpStatusCode from '../application/enums/http-statuses.enums';
import HttpException from '../application/exceptions/http-exception';
import { IStudent } from './types/student.interface';
import { AppDataSource } from '../configs/database/data-source';
import { Student } from './entities/student.entity';
import { DeepPartial, DeleteResult, UpdateResult } from 'typeorm';
import { groupsRepository } from '../groups/groups.service';

export const studentsRepository = AppDataSource.getRepository(Student);

export const getAllStudents = async (name?: string): Promise<Student[]> => {
  const result = studentsRepository
    .createQueryBuilder('student')
    .select([
      'student.id as id',
      'student.name as name',
      'student.surname as surname',
      'student.email as email',
      'student.age as age',
      'student.imagePath as "imagePath"',
    ])
    .orderBy('student.id')
    .leftJoin('student.group', 'group')
    .addSelect('group.name as "groupName"');
    

  if (name) {
    result.where('student.name = :name', { name });
  }

  const students = await result.getRawMany();
  return students;
};

export const getStudentById = async (id: string): Promise<Student> => {
  const student = await studentsRepository
    .createQueryBuilder('student')
    .select([
      'student.id as id',
      'student.name as name',
      'student.surname as surname',
      'student.email as email',
      'student.age as age',
      'student.imagePath as "imagePath"',
    ])
    .leftJoin('student.group', 'group')
    .addSelect('group.name as "groupName"')
    .where('student.id = :id', { id })
    .getRawOne();
  if (!student) {
    throw new HttpException(
      HttpStatusCode.NOT_FOUND,
      'Student with this id not found',
    );
  }
  return student;
};

export const createStudent = async (
  createStudentSchema: Omit<IStudent, 'id'>,
): Promise<Student> => {
  const student = await studentsRepository.findOne({
    where: [
      { email: createStudentSchema.email },
      { name: createStudentSchema.name, surname: createStudentSchema.surname },
    ],
  });
  if (student) {
    throw new HttpException(
      HttpStatusCode.CONFLICT,
      'Student with this email or name+surname already exists',
    );
  }
  return studentsRepository.save(createStudentSchema);
};

export const updateStudentById = async (
  id: string,
  updateStudentSchema: Partial<IStudent>,
): Promise<UpdateResult> => {
  const result = await studentsRepository.update(id, updateStudentSchema);
  if (!result.affected) {
    throw new HttpException(
      HttpStatusCode.NOT_FOUND,
      'Student with this id not found',
    );
  }
  return result;
};

export const deleteStudentById = async (id: string): Promise<DeleteResult> => {
  const result = await studentsRepository.delete(id);
  if (!result.affected) {
    throw new HttpException(
      HttpStatusCode.NOT_FOUND,
      'Student with this id not found',
    );
  }
  return result;
};

export const addStudentToGroup = async (
  studentId: string,
  groupId: string,
): Promise<Student> => {
  const student = await studentsRepository.findOne({
    where: {
      id: studentId,
    },
  });
  if (!student) {
    throw new HttpException(
      HttpStatusCode.NOT_FOUND,
      'Student with this id not found',
    );
  }
  const group = await groupsRepository.findOne({
    where: {
      id: groupId,
    },
  });

  if (!group) {
    throw new HttpException(
      HttpStatusCode.NOT_FOUND,
      'Group with this id not found',
    );
  }
  student.group = group;
  await studentsRepository.save(student);
  return student;
};
