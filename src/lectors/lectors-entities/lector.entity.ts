import { Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn, Unique } from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Course } from '../../courses/courses-entities/course.entity';
import { LectorCourse } from '../../lector_course/lector_course-entities/lector_course.entity';
import { Mark } from '../../marks/marks-entities/mark.entity';

@Entity({ name: 'lectors' })
export class Lector extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  email: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  password: string;

  @ManyToMany(() => LectorCourse, (lectorCourse) => lectorCourse.lector, { cascade: true })
  @JoinTable({ name: 'lector_course', joinColumn: { name: 'lector_id', referencedColumnName: "id" }, inverseJoinColumn: { name: 'course_id', referencedColumnName: "id" } })
  lectorCourses: LectorCourse[];

  @OneToMany(() => Mark, (mark) => mark.lector)
  marks: Mark[];
}
