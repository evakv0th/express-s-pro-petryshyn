import express from 'express';
import cors from 'cors';
import logger from './middlewares/logger.middleware';
import studentsRouter from '../students/students.router';
import { exceptionsFilter } from './middlewares/error.filter';
import { validatorURL } from './middlewares/URL-validator.middleware';
import path from 'path';
import auth from './middlewares/auth.middleware';
import bodyParser from 'body-parser';
import { AppDataSource } from '../configs/database/data-source';
import 'reflect-metadata';
import groupsRouter from '../groups/groups.router';
import coursesRouter from '../courses/courses.router';
import lectorsRouter from '../lectors/lectors.router';
import marksRouter from '../marks/marks.router';

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(logger);
app.use(auth);
AppDataSource.initialize()
  .then(() => {
    console.log('Typeorm connected to database');
  })
  .catch((err) => {
    console.log('Error typeorm didnt connect to DB', err);
  });

const staticFilesPath = path.join(__dirname, '../', 'public');

app.use('/api/v1/public', express.static(staticFilesPath));
app.use('/api/v1/students', studentsRouter);
app.use('/api/v1/groups', groupsRouter);
app.use('/api/v1/courses', coursesRouter);
app.use('/api/v1/lectors', lectorsRouter);
app.use('/api/v1/marks', marksRouter);

app.use(validatorURL);

app.use(exceptionsFilter);

export default app;
