import { NextFunction, Request, Response } from 'express';
import HttpException from '../exceptions/http-exception';
import HttpStatusCode from '../enums/http-statuses.enums';

export const validatorURL = (
  request: Request,
  response: Response,
  next: NextFunction,
) => {
  const validRoutes = ['/api/v1/students', '/api/v1/groups', '/api/v1/lectors', '/api/v1/courses', '/api/v1/marks'];

  const requestedUrl = request.originalUrl;

  if (!validRoutes.includes(requestedUrl)) {
    throw new HttpException(
      HttpStatusCode.NOT_FOUND,
      `Requested URL not found on server, it only supports: ${validRoutes[0]} OR ${validRoutes[1]} OR ${validRoutes[2]} OR ${validRoutes[3]} OR ${validRoutes[4]}`);
  } else {
    next();
  }
};
