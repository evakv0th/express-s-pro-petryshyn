import Joi from "joi";
import { ICourse } from "./types/courses.interface";

export const courseCreateSchema = Joi.object<Omit<ICourse, 'id'>>({
    name: Joi.string().required().min(2).max(20),
    description: Joi.string().required().min(5).max(100),
    hours: Joi.number().required().min(10)
})