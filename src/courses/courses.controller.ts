import { Request, Response } from 'express';
import * as coursesService from './courses.service';
import { ValidatedRequest } from 'express-joi-validation';
import HttpStatusCode from '../application/enums/http-statuses.enums';
import { ICourseCreateRequest } from './types/courses-create.schema';

export const getAllCourses = async (request: Request, response: Response) => {
  const courses = await coursesService.getAllCourses();
  response.json(courses);
};

export const getCourseById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const course = await coursesService.getCourseById(id);
  response.json(course);
};
export const createCourse = async (
  request: ValidatedRequest<ICourseCreateRequest>,
  response: Response,
) => {
  const course = await coursesService.createCourse(request.body);
  response
    .status(HttpStatusCode.CREATED)
    .json({ message: 'Created course', course: course });
};