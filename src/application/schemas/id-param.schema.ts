import Joi from 'joi';

export const idValidateSchema = Joi.object<{ id: string }>({
  id: Joi.number(),
});

export const idValidateForAddStudentToGroupSchema = Joi.object({
  id: Joi.number(),
  groupId: Joi.number(),
});

export const idValidateForAddLectorToCourseSchema = Joi.object({
  id: Joi.number(),
  courseId: Joi.number(),
});
