import { IGroup } from './types/group.interface';

import HttpStatusCode from '../application/enums/http-statuses.enums';
import HttpException from '../application/exceptions/http-exception';
import { AppDataSource } from '../configs/database/data-source';
import { Student } from '../students/entities/student.entity';
import { Group } from './entities/group.entity';
import { DeleteResult, UpdateResult } from 'typeorm';
import { studentsRepository } from '../students/students.service';

export const groupsRepository = AppDataSource.getRepository(Group);

export const getAllGroups = async (): Promise<Group[]> => {
  const groups = await groupsRepository
    .createQueryBuilder('group')
    .leftJoinAndSelect('group.students', 'student')
    .orderBy('group.id')
    .select([
      'group.id',
      'group.name',
      'student.name',
      'student.surname',
      'student.age',
      'student.email',
    ])
    .getMany();
  return groups;
};

export const getGroupById = async (id: string): Promise<Group> => {
  const group = await groupsRepository
    .createQueryBuilder('group')
    .leftJoinAndSelect('group.students', 'student')
    .where('group.id = :id', { id })
    .getOne();
  if (!group) {
    throw new HttpException(
      HttpStatusCode.NOT_FOUND,
      'Group with this id not found',
    );
  }
  return group;
};

export const createGroup = async (
  createGroupSchema: Omit<IGroup, 'id'>,
): Promise<Group> => {
  const group = await groupsRepository.findOne({
    where: {
      name: createGroupSchema.name,
    },
  });
  if (group) {
    throw new HttpException(
      HttpStatusCode.NOT_FOUND,
      'Group with this name already exists',
    );
  }
  return groupsRepository.save(createGroupSchema);
};

export const updateGroupById = async (
  id: string,
  updateGroupSchema: Partial<IGroup>,
): Promise<UpdateResult> => {
  const result = await groupsRepository.update(id, updateGroupSchema);
  if (!result.affected) {
    throw new HttpException(
      HttpStatusCode.NOT_FOUND,
      'Group with this id not found',
    );
  }
  return result;
};

export const deleteGroupById = async (id: string): Promise<DeleteResult> => {
  const groupId = parseInt(id);
  const students = await studentsRepository.find({
    where: {
      groupId: groupId
    }
  })
  if (students) {
    const updatePromises = students.map(async (student) => {
      student.groupId = null;
      await studentsRepository.save(student);
    });

    await Promise.all(updatePromises);
  }
  console.log(students)
  const result = await groupsRepository.delete(id);
  if (!result.affected) {
    throw new HttpException(
      HttpStatusCode.NOT_FOUND,
      'Group with this id not found',
    );
  }
  return result;
};
