import { Router } from 'express';
import * as marksController from './marks.controller';
import validator from '../application/middlewares/validation.middleware';
import { controllerWrapper } from '../application/utilities/controller-wrapper';
import {
  idValidateForAddLectorToCourseSchema,
  idValidateSchema,
} from '../application/schemas/id-param.schema'; 
import { MarkCreateSchema } from './marks.schema';

const router = Router();

router.get(
  '/student',
  controllerWrapper(marksController.getStudentMarks)
);
router.get(
    '/course',
    controllerWrapper(marksController.getCourseMarks)
  );

router.post(
  '/',
  validator.body(MarkCreateSchema),
  controllerWrapper(marksController.assignMarkToStudent),
);


export default router;