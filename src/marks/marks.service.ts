import HttpStatusCode from '../application/enums/http-statuses.enums';
import HttpException from '../application/exceptions/http-exception';
import { AppDataSource } from '../configs/database/data-source';
import { coursesRepository } from '../courses/courses.service';
import { lectorsRepository } from '../lectors/lectors.service';
import { studentsRepository } from '../students/students.service';
import { Mark } from './marks-entities/mark.entity';
import { IMark } from './types/marks.interface';

export const marksRepository = AppDataSource.getRepository(Mark);

export const getStudentMarks = async (studentId?: string): Promise<Mark[]> => {
  const result = marksRepository
    .createQueryBuilder('mark')
    .leftJoinAndSelect('mark.course', 'course')
    .leftJoinAndSelect('mark.student', 'student');
  if (studentId) {
    result.where('mark.student_id = :studentId', { studentId });
  }

  const marks = await result
  .select(['course.name as "courseName"', 'mark.mark as mark', 'student.id as "studentId"'])
  .orderBy('mark.createdAt')
  .getRawMany();

  return marks;
};

export const getCourseMarks = async (courseId?: string): Promise<Mark[]> => {
  const result = marksRepository.createQueryBuilder('mark');
  if (courseId) {
    result.where('mark.course_id = :courseId', { courseId });
  }

  const marks = await result
    .leftJoinAndSelect('mark.course', 'course')
    .leftJoinAndSelect('mark.lector', 'lector')
    .leftJoinAndSelect('mark.student', 'student')
    .select(['mark.mark as mark', 'course.name as "courseName"', 'student.name as "studentName"', 'lector.name as "lectorName"', 'course.id as "courseId"'])
    .orderBy('mark.createdAt')
    .getRawMany();

  return marks;
};

export const assignMarkToStudent = async (
  MarkCreateSchema: Omit<IMark, 'id'>,
): Promise<Mark> => {
  const { lectorId, studentId, courseId, ...markData } = MarkCreateSchema;

  const lector = await lectorsRepository.findOne({
    where: {
      id: lectorId,
    },
  });
  if (!lector) {
    throw new HttpException(
      HttpStatusCode.NOT_FOUND,
      'Lector with this id NOT FOUND',
    );
  }
  const course = await coursesRepository.findOne({
    where: {
      id: courseId,
    },
  });
  if (!course) {
    throw new HttpException(
      HttpStatusCode.NOT_FOUND,
      'Course with this id NOT FOUND',
    );
  }
  const student = await studentsRepository.findOne({
    where: {
      id: studentId,
    },
  });
  if (!student) {
    throw new HttpException(
      HttpStatusCode.NOT_FOUND,
      'Student with this id NOT FOUND',
    );
  }

  const mark = marksRepository.create(markData);
  mark.lector = lector;
  mark.course = course;
  mark.student = student;

  return marksRepository.save(mark);
};
