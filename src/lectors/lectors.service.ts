import { DataSource } from 'typeorm';
import HttpStatusCode from '../application/enums/http-statuses.enums';
import HttpException from '../application/exceptions/http-exception';
import { AppDataSource } from '../configs/database/data-source';
import { Course } from '../courses/courses-entities/course.entity';
import { coursesRepository } from '../courses/courses.service';
import { LectorCourse } from '../lector_course/lector_course-entities/lector_course.entity';
import { Lector } from './lectors-entities/lector.entity';
import { ILector, ILectorWithCourses } from './types/lectors.interface';

export const lectorsRepository = AppDataSource.getRepository(Lector);
export const lectorCourseRepository = AppDataSource.getRepository(LectorCourse);

export const getAllLectors = async (): Promise<Lector[]> => {
  const lectors = await lectorsRepository.find({});

  return lectors;
};

export const getLectorById = async (id: string): Promise<Lector> => {
  const lector = await lectorsRepository
    .createQueryBuilder('lector')
    .select([
      'lector.id as id',
      'lector.name as name',
      'lector.email as email',
      'lector.password as password',
    ])
    .where('lector.id = :id', { id })
    .getRawOne();

  if (!lector) {
    throw new HttpException(
      HttpStatusCode.NOT_FOUND,
      'Lector with this id not found',
    );
  }

  const courses = await lectorCourseRepository
    .createQueryBuilder('lectorCourse')
    .leftJoinAndSelect('lectorCourse.lector', 'lector')
    .leftJoinAndSelect('lectorCourse.course', 'course')
    .select([
      'course.id as "courseId"',
      'course.name as "courseName"',
      'course.description as "courseDescription"',
      'course.hours as "courseHours"',
      'course.created_at as "createdAt"',
      'course.updated_at as "updatedAt"',
    ])
    .where('lector.id = :id', { id })
    .getRawMany();

  console.log(`getLectorById with ${id} id of an lector`);
  lector.courses = courses;

  return lector;
};

export const createLector = async (
  lectorCreateSchema: Omit<ILector, 'id'>,
): Promise<Lector> => {
  const lector = await lectorsRepository.findOne({
    where: [
      { name: lectorCreateSchema.name },
      { email: lectorCreateSchema.email },
    ],
  });
  if (lector) {
    throw new HttpException(
      HttpStatusCode.NOT_FOUND,
      'Lector with this name or email already exists',
    );
  }
  return lectorsRepository.save(lectorCreateSchema);
};

export const addLectorToCourse = async (
  lectorId: string,
  courseId: string,
): Promise<ILectorWithCourses> => {
  const lector = await lectorsRepository.findOne({
    where: {
      id: lectorId,
    },
  });
  if (!lector) {
    throw new HttpException(
      HttpStatusCode.NOT_FOUND,
      'Lector with this id not found',
    );
  }
  const course = await coursesRepository.findOne({
    where: {
      id: courseId,
    },
  });

  if (!course) {
    throw new HttpException(
      HttpStatusCode.NOT_FOUND,
      'Course with this id not found',
    );
  }
  const lectorCourse = new LectorCourse();
  lectorCourse.lector = lector;
  lectorCourse.course = course;

  const courses = await lectorCourseRepository
    .createQueryBuilder('lectorCourse')
    .leftJoinAndSelect('lectorCourse.lector', 'lector')
    .leftJoinAndSelect('lectorCourse.course', 'course')
    .select(['course.id', 'course.name'])
    .where('lector.id = :lectorId', { lectorId })
    .getRawMany();

  await lectorCourseRepository.save(lectorCourse);

  return { ...lector, courses: courses };
};

export const getLectorCourses = async (id: string): Promise<Lector[]> => {
  const lector = await lectorCourseRepository
    .createQueryBuilder('lectorCourse')
    .leftJoinAndSelect('lectorCourse.lector', 'lector')
    .leftJoinAndSelect('lectorCourse.course', 'course')
    .select([
      'lector.name as name',
      'course.id as "courseId"',
      'course.name as "courseName"',
    ])
    .where('lector.id = :id', { id })
    .getRawMany();

  if (!lector) {
    throw new HttpException(
      HttpStatusCode.NOT_FOUND,
      'Lector with this id not found',
    );
  }
  return lector;
};
