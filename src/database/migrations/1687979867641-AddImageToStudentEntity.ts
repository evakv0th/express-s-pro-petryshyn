import { MigrationInterface, QueryRunner } from "typeorm";

export class AddImageToStudentEntity1687979867641 implements MigrationInterface {
    name = 'AddImageToStudentEntity1687979867641'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "students" ADD "imagePath" character varying`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "students" DROP COLUMN "imagePath"`);
    }

}
