import Joi from 'joi';
import { IStudent } from './types/student.interface';

export const studentCreateSchema = Joi.object<Omit<IStudent, 'id'>>({
  name: Joi.string().required().min(3).max(30),
  surname: Joi.string().required().min(3).max(30),
  email: Joi.string()
    .required()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }),
  age: Joi.number().required().min(14).max(99),
  imagePath: Joi.string().optional()
});

export const studentUpdateSchema = Joi.object<Partial<IStudent>>({
  name: Joi.string().optional().min(3).max(30),
  surname: Joi.string().optional().min(3).max(30),
  email: Joi.string()
    .optional()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }),
  age: Joi.number().optional().min(14).max(99),
  imagePath: Joi.string().optional(),
  groupId: Joi.string().optional()
});
