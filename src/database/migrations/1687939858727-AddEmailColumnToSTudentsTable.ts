import { MigrationInterface, QueryRunner } from "typeorm";

export class AddEmailColumnToSTudentsTable1687939858727 implements MigrationInterface {
    name = 'AddEmailColumnToSTudentsTable1687939858727'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "students" ADD "email" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "students" ALTER COLUMN "name" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "students" ALTER COLUMN "surname" SET NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "students" ALTER COLUMN "surname" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "students" ALTER COLUMN "name" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "students" DROP COLUMN "email"`);
    }

}
