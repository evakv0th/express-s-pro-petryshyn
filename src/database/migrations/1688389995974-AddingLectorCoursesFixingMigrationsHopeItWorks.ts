import { MigrationInterface, QueryRunner } from "typeorm";

export class AddingLectorCoursesFixingMigrationsHopeItWorks1688389995974 implements MigrationInterface {
    name = 'AddingLectorCoursesFixingMigrationsHopeItWorks1688389995974'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "lector_course" ("id" SERIAL NOT NULL, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "lector_id" integer, "course_id" integer, CONSTRAINT "PK_06fddf84d2c08e616f20aa4c658" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE INDEX "IDX_5226e1592e6291dbe7a0764034" ON "marks" ("student_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_3e39a10631f1c639777a2b99cb" ON "marks" ("course_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_b5e856b621a7b64cdf48059067" ON "students" ("name") `);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "PK_06fddf84d2c08e616f20aa4c658"`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "PK_79cf0f064235769277ce94c75f7" PRIMARY KEY ("lector_id", "course_id")`);
        await queryRunner.query(`ALTER TABLE "lector_course" ALTER COLUMN "lector_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "lector_course" ALTER COLUMN "course_id" SET NOT NULL`);
        await queryRunner.query(`CREATE INDEX "IDX_fa21194644d188132582b0d1a3" ON "lector_course" ("lector_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_67ca379415454fe43871951552" ON "lector_course" ("course_id") `);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "FK_fa21194644d188132582b0d1a3f" FOREIGN KEY ("lector_id") REFERENCES "lectors"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "FK_67ca379415454fe438719515529" FOREIGN KEY ("course_id") REFERENCES "courses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "FK_67ca379415454fe438719515529"`);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "FK_fa21194644d188132582b0d1a3f"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_67ca379415454fe43871951552"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_fa21194644d188132582b0d1a3"`);
        await queryRunner.query(`ALTER TABLE "lector_course" ALTER COLUMN "course_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "lector_course" ALTER COLUMN "lector_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "PK_79cf0f064235769277ce94c75f7"`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "PK_06fddf84d2c08e616f20aa4c658" PRIMARY KEY ("id")`);
        await queryRunner.query(`DROP INDEX "public"."IDX_b5e856b621a7b64cdf48059067"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_3e39a10631f1c639777a2b99cb"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_5226e1592e6291dbe7a0764034"`);
        await queryRunner.query(`DROP TABLE "lector_course"`);
    }

}
