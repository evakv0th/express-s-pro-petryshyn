import { Router } from 'express';
import * as studentsController from './students.controller';
import { controllerWrapper } from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import { studentCreateSchema, studentUpdateSchema } from './students.schema';
import {
  idValidateForAddStudentToGroupSchema,
  idValidateSchema,
} from '../application/schemas/id-param.schema';

const router = Router();


router.get('/', controllerWrapper(studentsController.getAllStudents));
router.get(
  '/:id',
  validator.params(idValidateSchema),
  controllerWrapper(studentsController.getStudentById),
);
router.post(
  '/',
  validator.body(studentCreateSchema),
  controllerWrapper(studentsController.createStudent),
);
router.patch(
  '/:id/groups/:groupId',
  validator.params(idValidateForAddStudentToGroupSchema),
  controllerWrapper(studentsController.addStudentToGroup),
);
router.patch(
  '/:id',
  validator.params(idValidateSchema),
  validator.body(studentUpdateSchema),
  controllerWrapper(studentsController.updateStudentById),
);
router.delete(
  '/:id',
  validator.params(idValidateSchema),
  controllerWrapper(studentsController.deleteStudentById),
);

export default router;
