import { MigrationInterface, QueryRunner } from 'typeorm';

export class ChangeAgeInsteadOfSalaryInStudent1687953893710
  implements MigrationInterface
{
  name = 'ChangeAgeInsteadOfSalaryInStudent1687953893710';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "students" DROP COLUMN "salary"`);
    await queryRunner.query(`ALTER TABLE "students" ADD "age" numeric`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "students" DROP COLUMN "age"`);
    await queryRunner.query(`ALTER TABLE "students" ADD "salary" numeric`);
  }
}
