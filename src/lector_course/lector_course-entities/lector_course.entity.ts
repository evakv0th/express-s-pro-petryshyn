import { Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Lector } from '../../lectors/lectors-entities/lector.entity';
import { Course } from '../../courses/courses-entities/course.entity';

@Entity({ name: 'lector_course' })
export class LectorCourse extends CoreEntity {

    @ManyToOne(() => Lector, (lector) => lector.lectorCourses)
    @JoinColumn({ name: 'lector_id' })
    lector: Lector;
  
    @ManyToOne(() => Course, (course) => course.lectorCourses)
    @JoinColumn({ name: 'course_id' })
    course: Course;
}
