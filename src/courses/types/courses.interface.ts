
export interface ICourse{
    name: string,
    description: string,
    hours: number 
}