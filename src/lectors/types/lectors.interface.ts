import { Course } from "../../courses/courses-entities/course.entity";

export interface ILector{
    name: string,
    email: string,
    password: string 
}

export interface ILectorWithCourses{
    courses: Course[];
}
