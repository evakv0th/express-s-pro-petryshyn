import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { LectorCourse } from '../../lector_course/lector_course-entities/lector_course.entity';
import { Mark } from '../../marks/marks-entities/mark.entity';

@Entity({ name: 'courses' })
export class Course extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  description: string;

  @Column({
    type: 'integer',
    nullable: false,
  })
  hours: number;

  @ManyToMany(() => LectorCourse, (lectorCourse) => lectorCourse.course, { cascade: true })
  lectorCourses: LectorCourse[];

  @OneToMany(() => Mark, (mark) => mark.course)
  marks: Mark[];
}
