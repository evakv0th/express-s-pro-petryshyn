import HttpStatusCode from '../application/enums/http-statuses.enums';
import HttpException from '../application/exceptions/http-exception';
import { AppDataSource } from '../configs/database/data-source';
import { DeleteResult, UpdateResult } from 'typeorm';
import { Course } from './courses-entities/course.entity';
import { ICourse } from './types/courses.interface';

export const coursesRepository = AppDataSource.getRepository(Course);

export const getAllCourses = async (): Promise<Course[]> => {
  const courses = await coursesRepository.find({});
  return courses;
};

export const getCourseById = async (id: string): Promise<Course> => {
  const course = await coursesRepository
    .createQueryBuilder('course')
    .where('course.id = :id', { id })
    .getOne();
  if (!course) {
    throw new HttpException(
      HttpStatusCode.NOT_FOUND,
      'Course with this id not found',
    );
  }
  return course;
};

export const createCourse = async (courseCreateSchema: Omit<ICourse, 'id'>): Promise<Course> => {
  const course = await coursesRepository.findOne({
    where: [
      { name: courseCreateSchema.name },
      { description: courseCreateSchema.description },
    ]
  });
  if (course) {
    throw new HttpException(
      HttpStatusCode.NOT_FOUND,
      'Course with this name or description already exists',
    );
  }
  return coursesRepository.save(courseCreateSchema);
};


