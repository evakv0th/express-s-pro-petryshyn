# README для людини, яка буде перевіряти ДЗ.

## Functionality:
- отримати всіх студентів (кожен студент повинен мати ім’я групи) - `GET 'api/v1/students'`
- отримати студента за id (студент повинен мати ім’я групи) - `GET 'api/v1/students/:id'`
- додати студента - `POST 'api/v1/students'`
- оновити дані студента - `PATCH 'api/v1/students/:id'`
- видалити студента - `DELETE 'api/v1/students/:id'`
- додати групу студенту - `'api/v1/students/:studentId/groups/:groupId'`
- отримати всі групи (кожна група повинна містити масив студентів які їй належать) - `GET 'api/v1/groups'`
- отримати групу за id (група повинна містити масив студентів які їй належать) - `GET 'api/v1/groups/:id'`
- додати групу - `POST 'api/v1/groups'`
- оновити дані групи - `PATCH 'api/v1/groups/:id'`
- видалити групу - `DELETE 'api/v1/groups/:id'` **(if group has students, assigns students groupID to NULL and deletes the group)**
- створення лектора - `POST '/api/v1/lectors'`
- створення курсу - `POST '/api/v1/courses'`
- додавання оцінки для студента (body має містити course_id, student_id, lector_id) - `POST 'api/v1/marks'`
- додавання лектора до курсу - `PATCH 'lectors/:lectorId/courses/:courseId'`
- отримати всіх лекторів - `GET '/api/v1/lectors'` **(without courses)**
- отримати лектора по lector_id із всіма курсами на яких він викладає (має бути вся інформація про лектора із масивом студентів) - `GET '/api/v1/lectors/:id'` **(with courses)**
- отримати всі курси - `GET '/api/v1/courses'`
- отримати всі оцінки студента по student_id (має містити назву курсу і оцінку) - `GET 'api/v1/marks/student?student_id=:studentId'` **(it can be used with index to filter by student id, or just get all marks for all students without index)**
- отримати всі оцінки по курсу використовуючи course_id (має містити назву курсу, ім’я лектора, хто поставив оцінку, ім’я студента та саму оцінку) - `GET 'api/v1/marks/course?course_id=:courseId'` **(it can be used with index to filter by course id, or just get all marks for all courses without index)**
- отримати всі курси на яких веде лектор по lector_id - `GET 'api/v1/lectors/:lectorId/courses'`
- додати індекс для поля name в таблиці students. Оновити ендпоінт GET /students добавити query param `name` і зробити пошук по name. Запит має виглядати GET /students?name=Andrew - `GET 'api/v1/students?name=:name'` **(its working)**
