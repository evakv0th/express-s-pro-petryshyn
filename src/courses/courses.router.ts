import { Router } from 'express';
import * as coursesController from './courses.controller';
import validator from '../application/middlewares/validation.middleware';
import { controllerWrapper } from '../application/utilities/controller-wrapper';
import {
  idValidateSchema,
} from '../application/schemas/id-param.schema';
import { courseCreateSchema } from './courses.schema';

const router = Router();

router.get('/', controllerWrapper(coursesController.getAllCourses));
router.get(
  '/:id',
  validator.params(idValidateSchema),
  controllerWrapper(coursesController.getCourseById),
);
router.post(
  '/',
  validator.body(courseCreateSchema),
  controllerWrapper(coursesController.createCourse),
);


export default router;