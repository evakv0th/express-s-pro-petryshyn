import { IStudent, IStudentWithGroupName } from "../../students/types/student.interface";

export interface IGroup{
    id: string,
    name: string 
}

export interface IGroupWithStudents extends IGroup {
    studentsInGroup?: IStudentWithGroupName[];
}