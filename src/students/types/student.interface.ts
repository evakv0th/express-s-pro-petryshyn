import { Group } from "../../groups/entities/group.entity";

export interface IStudent {
    id: string;
    email: string;
    name: string;
    surname: string;
    age: number;
    groupId: number | null;
    group: Group;
    imagePath: string;
}


export interface IStudentWithGroupName extends IStudent {
    groupName: string | null;
}
