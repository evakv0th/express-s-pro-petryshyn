import { Request, Response } from 'express';
import * as groupsService from './groups.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IGroupUpdateRequest } from './types/groups-update-schema';
import { IGroupCreateRequest } from './types/groups-create-schema';
import HttpStatusCode from '../application/enums/http-statuses.enums';

export const getAllGroups = async (request: Request, response: Response) => {
  const groups = await groupsService.getAllGroups();
  response.json(groups);
};

export const getGroupById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const group = await groupsService.getGroupById(id);
  response.json(group);
};
export const createGroup = async (
  request: ValidatedRequest<IGroupCreateRequest>,
  response: Response,
) => {
  const group = await groupsService.createGroup(request.body);
  response
    .status(HttpStatusCode.CREATED)
    .json({ message: 'Created group', group: group });
};

export const updateGroupById = async (
  request: ValidatedRequest<IGroupUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const group = await groupsService.updateGroupById(id, request.body);
  response.json({ message: 'updated group', group: group });
};

export const deleteGroupById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const group = await groupsService.deleteGroupById(id);
  response.json(group);
};
