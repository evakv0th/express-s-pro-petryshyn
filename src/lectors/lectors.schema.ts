import Joi from "joi";
import { ILector } from "./types/lectors.interface";

export const lectorCreateSchema = Joi.object<Omit<ILector, 'id'>>({
    name: Joi.string().required().min(2).max(20),
    email: Joi.string().required().min(5).max(100),
    password: Joi.string().required()
})